<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Costumers extends Model
{
    //
     protected $table = 'costumers';
     protected $fillable=['name','address','rfc','user_id'];

    public function Users(){

    	return $this->hasMany('App/User');
    }
}
