<?php

namespace App\Http\Controllers;
use App\Costumers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $users = Costumers::select('name', 'address as user_email','address','rfc')
         ->paginate(5);
        //$users=User::orderBy("id",'ASC')->paginate(5);//muestra todos los usuarios
        return view('admin.users.index')->with('users',$users);
    }
}
