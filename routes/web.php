<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::group(['prefix'=> 'admin'],function(){
	Route::resource('users', 'UserController');//tomar los metodos de un controlador y lo obtine como ruta
	/*Route::get('users/{id}/destroy',[//ruta para eliminar
			'uses'=>'UserController@destroy',
			'as'=>'admin.users.destroy'
		]);*/

		Route::get('users/{id}/destroy',[//ruta para eliminar

			'uses'=>'UserController@destroy',
			'as'=>'admin.users.destroy'
		]);
});




Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
